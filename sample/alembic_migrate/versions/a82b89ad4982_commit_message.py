"""COMMIT_MESSAGE

Revision ID: a82b89ad4982
Revises:
Create Date: 2023-01-04 16:20:23.606361

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a82b89ad4982"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "category",
        sa.Column("catid", sa.SmallInteger(), nullable=False),
        sa.Column("catgroup", sa.String(length=10), nullable=True),
        sa.Column("catname", sa.String(length=10), nullable=True),
        sa.Column("catdesc", sa.String(length=50), nullable=True),
    )
    op.create_table(
        "users",
        sa.Column("userid", sa.Integer(), nullable=False),
        sa.Column("username", sa.CHAR(length=8), nullable=True),
        sa.Column("firstname", sa.String(length=30), nullable=True),
        sa.Column("lastname", sa.String(length=30), nullable=True),
        sa.Column("city", sa.String(length=30), nullable=True),
        sa.Column("state", sa.CHAR(length=2), nullable=True),
        sa.Column("email", sa.String(length=100), nullable=True),
        sa.Column("phone", sa.CHAR(length=14), nullable=True),
        sa.Column("likesports", sa.Boolean(), nullable=True),
        sa.Column("liketheatre", sa.Boolean(), nullable=True),
        sa.Column("likeconcerts", sa.Boolean(), nullable=True),
        sa.Column("likejazz", sa.Boolean(), nullable=True),
        sa.Column("likeclassical", sa.Boolean(), nullable=True),
        sa.Column("likeopera", sa.Boolean(), nullable=True),
        sa.Column("likerock", sa.Boolean(), nullable=True),
        sa.Column("likevegas", sa.Boolean(), nullable=True),
        sa.Column("likebroadway", sa.Boolean(), nullable=True),
        sa.Column("likemusicals", sa.Boolean(), nullable=True),
    )
    op.create_table(
        "venue",
        sa.Column("venueid", sa.SmallInteger(), nullable=False),
        sa.Column("venuename", sa.String(length=100), nullable=True),
        sa.Column("venuecity", sa.String(length=30), nullable=True),
        sa.Column("venuestate", sa.CHAR(length=2), nullable=True),
        sa.Column("venueseats", sa.Integer(), nullable=True),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("venue")
    op.drop_table("users")
    op.drop_table("category")
    # ### end Alembic commands ###
